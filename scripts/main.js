var worldWidth = 4*1280;
var worldHeight = 4*720;
var seaColor = "#339CFF";
var twopi = Math.PI*2;

var fish=[];
var algae=[];
var speciesList = {};
var fishGrid = [];
var eatenFish = [];

var sensorDistance=500;
var gridSize=sensorDistance;

var fishSize = 10;
var swimSpeed = 5;
var maxFishSize = 25;
var twopi = Math.PI*2;
var maxLifeForce = 2500;

var maxPop=0;

var fossilRecord = [];

function onLoad(){
    canvas          = document.getElementById("GUICanvas");
    canvas.width    = worldWidth;
    canvas.height   = worldHeight;
    ctx             = canvas.getContext("2d");
    ctx.font = "30px Arial";
    canvas.addEventListener("mousedown", function(event){
        var rect = canvas.getBoundingClientRect();
        var ratiox = rect.width/worldWidth;
        var ratioy = rect.height/worldHeight;
        var x = event.x/ratiox;
        var y = event.y/ratioy;
        fish[fish.length]=generateFish(x,y);
    });
    loadVariables();
    // Start Game
    window.requestAnimationFrame(gameLoop);
}

function loadVariables(){
    pregnancyDistance       = Number(pregnancyValue.value);
    fishSize                = Number(fishSizeValue.value);
    swimSpeed               = Number(swimSpeedValue.value);
    maxFishSize             = Number(maxFishSizeValue.value);
    maxLifeForce            = Number(maxLifeForceValue.value);
}

function gameLoop(){
    countFish();
    drawSea();
    generateAlage();
    drawAlgae();
    drawFish();
    drawGraph();
    if (document.getElementById("calculate").checked){
        calculateFish();
    }

    ctx.fillStyle = "black";
    ctx.fillText(("Number of fish: " + fish.length)  , 10, 50);
    ctx.fillText(("Number of species: " + Object.keys(speciesList).length)  , 10, 75);
    ctx.fillText(("Number of algae: " + algae.length)  , 10, 100);

    moveFish();
    loadVariables();
    window.requestAnimationFrame(gameLoop);
}

// GENERATE

function generateFish(x=worldWidth*Math.random(),y=worldHeight*Math.random(),r=Math.floor(16777215*Math.random()).toString(16)){
    while(r.length<6){r = "0" + r;}
    while(r.length<7){r = "#" + r;}
    localFish={
        x:x,
        y:y,
        size:fishSize*Math.random(),
        swimSpeed:swimSpeed*Math.random(),
        currentSwimSpeed:this.swimSpeed,
        swimDirection:Math.random()*2*Math.PI,
        inactiveTime:Math.floor(500*Math.random()),
        sensor:sensorDistance,
        seesFood:false,
        pregnant:false,
        grid:{},
        eaten:false,
        pregnantTimer:0,
        targetDistance:99999,
        conformity:Math.random(),
        color: r,
        lifeForce:Math.floor(maxLifeForce*(Math.random()+0.5)),
        gender:selectGender()
    }
    return localFish;
}

function selectGender(){
    if(Math.random()<0.5){return "female";}else{return "male";}
}

function generateAlage(){
    if(algae.length<Number(maxAlgaeValue.value)){
        algae[algae.length]={
            x:Math.round(Math.random()*worldWidth),
            y:Math.round(Math.random()*worldHeight)
        }
    }
}

// CALCULATE

function absoluteDistance(point1,point2){
    xdist = point1.x - point2.x;
    ydist = point1.y - point2.y;
    return Math.sqrt((xdist*xdist) + (ydist*ydist));
}

function countFish(){
    speciesList={};
    for(i=0;i<fish.length;i++){
    // if (fish[i].color!="#f0f0f0"){
        if(fish[i].color in speciesList){
            speciesList[fish[i].color]+=1;
        }else{
            speciesList[fish[i].color]=1;
        }
    // }
    }
}

function calculateFish(){
    fishGrid.length=0;
    resolutionW=worldWidth/gridSize;
    resolutionH=worldHeight/gridSize;
    for(i=-1;i<resolutionW+1;i++){
        fishGrid[i] = [];
        for(j=-1;j<resolutionH+1;j++){
            fishGrid[i][j] = [];
        }
    }
    for(i=0;i<fish.length;i++){
        x=Math.floor(Math.abs(fish[i].x) / gridSize);
        y=Math.floor(Math.abs(fish[i].y) / gridSize);
        fishGrid[x][y].push(i);
        fish[i].grid={x:x,y:y};
    }
    eatenFish=[];
    for(i=fish.length-1;i>=0;i--){
        if(fish[i].eaten){continue;}
        // SETUP
        fish[i].targetDistance=99999;
        fish[i].lifeForce--;
        // DEAD FISH
        if(fish[i].lifeForce<0){
            fish[i].swimDirection=Math.PI*1.5;
            fish[i].color="#f0f0f0";
            fish[i].swimSpeed=5;
            fish[i].currentSwimSpeed=5;
            fish[i].seesFood=false;
        }
        //LIVE FISH
        else{
            fish[i].seesFood=false;
            // PREGNANT FISH
            if(fish[i].gender=="female" && fish[i].pregnant){
                fish[i].pregnantTimer--;
                if(fish[i].pregnantTimer<0){
                    children = Math.floor(100*Math.random());
                    console.log(i + " gave birth to " + children + " children")
                    for(j=0;j<children;j++){fish[fish.length]=generateFish(fish[i].x,fish[i].y,r=fish[i].color)}
                    fish[i].pregnant=false;
                }
            }
            // NON-PREGNANT FISH
            for(j=algae.length-1;j>=0 && fish[i].inactiveTime<0;j--){
                if(Math.abs(algae[j].x-fish[i].x)<fish[i].sensor){
                    absDist = absoluteDistance(fish[i],algae[j]);
                    if(absDist<2+fish[i].swimSpeed){
                            fish[i].size+= (1-(fish[i].size/maxFishSize))*fish[i].size;
                            fish[i].inactiveTime=250;
                            if(fish[i].gender=="female" && Math.random()>Number(pregnancyValue.value)){console.log(i + " got pregnant");fish[i].pregnantTimer=500;fish[i].pregnant=true;}
                            algae.splice(j, 1);
                            fish[i].lifeForce=maxLifeForce;
                            break;}
                    if(fish[i].targetDistance>absDist && absDist<fish[i].sensor){
                        fish[i].targetDistance=absDist;
                        fish[i].swimDirection = Math.atan2(algae[j].y - fish[i].y, algae[j].x - fish[i].x);
                        if (document.getElementById("foodLine").checked){
                            drawLine({x:fish[i].x, y:fish[i].y}, {x:algae[j].x, y:algae[j].y}, "#FF0000", 1);
                        }
                        fish[i].seesFood=true;
                        fish[i].currentSwimSpeed = fish[i].swimSpeed;
                    }
                }
            }
            // for (j=fish.length-1; j>=0; j--){
            for(l=-1;l<2;l++){
                for(k=-1;k<2;k++){
                    for(m=fishGrid[fish[i].grid.x-l][fish[i].grid.y-k].length-1; m>=0; m--){
                        j=fishGrid[fish[i].grid.x-l][fish[i].grid.y-k][m];
                        if (j != i){
                            // console.log(i,j);
                            absDist = absoluteDistance(fish[i],fish[j]);
                            if(absDist < fish[i].sensor){
                                if (document.getElementById("ownSize").checked){q=fish[j].size;}
                                else{q=fish[j].size+1}
                                if(q<=fish[i].size && fish[i].size/4<fish[j].size && fish[i].inactiveTime<0 && fish[i].color!=fish[j].color && fish[i].targetDistance>absDist){
                                    fish[i].targetDistance=absDist;
                                    fish[i].swimDirection = Math.atan2(fish[j].y - fish[i].y, fish[j].x - fish[i].x);
                                    if (document.getElementById("foodLine").checked){
                                        drawLine({x:fish[i].x, y:fish[i].y}, {x:fish[j].x, y:fish[j].y}, "#FF0000",1);
                                    }
                                    fish[i].seesFood=true;
                                    fish[i].currentSwimSpeed = fish[i].swimSpeed;
                                    if(absDist<fish[j].size){
                                        fish[i].size+= Math.round((1-(fish[i].size/maxFishSize))*fish[i].size);
                                        fish[i].inactiveTime=250;
                                        if(fish[i].gender=="female" && fish[j].gender=="male" && Math.random()>Number(pregnancyValue.value)){console.log(i + " got pregnant");fish[i].pregnantTimer=500;fish[i].pregnant=true;}
                                        // console.log("l ", l, " k ",k, " m ",m, " j ", j);
                                        // console.log(fish[j]);
                                        // console.log(fishGrid[fish[i].grid.x-l][fish[i].grid.y-k]);
                                        // // fish.splice(j, 1);
                                        // fishGrid[fish[i].grid.x-l][fish[i].grid.y-k].splice(m,1);
                                        // console.log(fish[j]);
                                        // console.log(fishGrid[fish[i].grid.x-l][fish[i].grid.y-k]);
                                        // if(i>j){i--};
                                        eatenFish[eatenFish.length]=j;
                                        fish[j].eaten=true;
                                        fish[i].lifeForce=maxLifeForce;
                                        continue;
                                    }
                                }
                                else if (!fish[i].seesFood){
                                    if (fish[i].conformity < fish[j].conformity && fish[i].color==fish[j].color){
                                        fish[i].swimDirection = (fish[i].swimDirection + fish[j].swimDirection)/2;
                                        fish[i].currentSwimSpeed = fish[j].swimSpeed;
                                        if (document.getElementById("sensorLine").checked){
                                            ctx.lineWidth = 1;
                                            drawLine({x:fish[i].x, y:fish[i].y}, {x:fish[j].x, y:fish[j].y}, "#00ff00");
                                        }
                                    }
                                }
                            }j--;
                        }
                    }
                }
            }fish[i].inactiveTime--;
        }
    }
    for(i=0;i<eatenFish.length;i++){
        fish.splice(eatenFish[i], 1);
    }
}

// MOVE

function moveFish(){
    if(fish.length>0){
        for (i=fish.length-1;i>=0;i--){
            sprint=1;
            if (fish[i].x<(0-fish[i].size*3)){
                fish[i].x = worldWidth;
            }
            else if (fish[i].x>(worldWidth+fish[i].size*3)){
                fish[i].x = 0;
            }
            if (fish[i].y<(0-fish[i].size*3)){
                if(fish[i].lifeForce<0){fish.splice(i,1);console.log(i + " died");continue;}
                fish[i].y = worldHeight;
            }
            else if (fish[i].y>(worldHeight+fish[i].size*3)){
                fish[i].y = 0;
            }
            if(fish[i].seesFood){sprint=1.5;}
            fish[i].x = Math.round(fish[i].x + Math.cos(fish[i].swimDirection)*fish[i].currentSwimSpeed*sprint);
            fish[i].y = Math.round(fish[i].y + Math.sin(fish[i].swimDirection)*fish[i].currentSwimSpeed*sprint);
        }
    }
}

// DRAWING
function drawSea(){
    ctx.fillStyle =  seaColor;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    if (document.getElementById("grid").checked){
        resolutionW=worldWidth/gridSize;
        resolutionH=worldHeight/gridSize;
        for(i=0;i<resolutionW;i++){
            drawLine({x:i*gridSize,y:0}, {x:i*gridSize,y:worldHeight}, "#000000");
        }
        for(i=0;i<resolutionH;i++){
            drawLine({x:0,y:i*gridSize}, {x:worldWidth,y:i*gridSize}, "#000000");
        }
    }
}

function drawAlgae(){
    if(algae.length>0){
        for(i=0;i<algae.length;i++){
            drawCircle(algae[i],2,"#00ff00");
        }
    }
}

function drawFish(){
    for (i=0;i<fish.length;i++){
        ctx.beginPath();
        x=fish[i].x;
        y=fish[i].y;
        size=fish[i].size;
        direction=fish[i].swimDirection;
        a={x:x,y:y};
        if (document.getElementById("drawFish").checked){
        b={x:(a.x-Math.cos(direction-(Math.PI*0.25))*size),      y:(a.y-Math.sin(direction-(Math.PI*0.25))*size)};
        e={x:(a.x-Math.cos(direction+(Math.PI*0.25))*size),      y:(a.y-Math.sin(direction+(Math.PI*0.25))*size)};
        c={x:(b.x-Math.cos(direction+(Math.PI*0.25))*size*2),    y:(b.y-Math.sin(direction+(Math.PI*0.25))*size*2)};
        d={x:(e.x-Math.cos(direction-(Math.PI*0.25))*size*2),    y:(e.y-Math.sin(direction-(Math.PI*0.25))*size*2)};
        
        ctx.moveTo(a.x,a.y);
        ctx.lineTo(b.x,b.y);
        ctx.lineTo(c.x,c.y);
        ctx.lineTo(d.x,d.y);
        ctx.lineTo(e.x,e.y);
        ctx.lineTo(a.x,a.y);
        
        ctx.fillStyle = fish[i].color;}else{
        drawCircle(a,size,fish[i].color);}
        
        if (document.getElementById("outline").checked){
            ctx.lineWidth = 2;
            ctx.strokeStyle = "#000000";
            ctx.stroke();}
            ctx.fill();
        if (document.getElementById("sensorDistance").checked){
            ctx.beginPath();
            ctx.arc(fish[i].x, fish[i].y, fish[i].sensor, 0, twopi, true);
            ctx.strokeStyle = "#000000";
            ctx.lineWidth = 1;
            ctx.stroke();
        }
        if (document.getElementById("fishInfo").checked){
            ctx.fillStyle = "black";
            ctx.fillText((i + " " + fish[i].lifeForce), fish[i].x, fish[i].y);}
    }

}

function drawLine(a, b, color, width=10){
    ctx.beginPath();
    ctx.moveTo(a.x, a.y);
    ctx.lineTo(b.x, b.y);
    ctx.strokeStyle = color;
    ctx.lineWidth = width;
    ctx.stroke();
}

function drawCircle(a, radius, color){
    ctx.beginPath();
    ctx.arc(a.x, a.y, radius, 0, twopi, true);
    ctx.fillStyle = color;
    ctx.fill();
}
function DisplayChange(newvalue,element) {
    document.getElementById(element).innerHTML = newvalue;
}

function drawGraph(){
    if (document.getElementById("pieChart").checked){
        x=270;
        y=390;
        radius=200;
        angle=0;
        list = Object.keys(speciesList).sort();
        for(species in list){
            sp=list[species];
            ctx.beginPath();
            ctx.moveTo(x, y);
            angle1=angle;
            angle=angle+(speciesList[sp]/fish.length)*twopi;
            ctx.arc(x,y,radius,angle1,angle);
            ctx.lineTo(x,y);
            ctx.fillStyle=sp;
            ctx.fill();
            ctx.fillStyle=sp;
            ctx.fillText((((speciesList[sp]/fish.length)*100).toFixed(0) + "%"), (Math.cos(angle)*(radius+70)+x), (Math.sin(angle)*(radius+70)+y));
            if (document.getElementById("outline").checked){
                ctx.stroke();
            }
        }
    }
    fossilRecord[fossilRecord.length]=speciesList;
    if (document.getElementById("lineGraph").checked){
        rx=0;
        ry=y+radius+90;
        width=400;
        height=400;
        for(species in speciesList){
            if(speciesList[species]>1){
                i=0;
                ctx.beginPath();
                ctx.moveTo(rx,ry+height);
                for(year in fossilRecord){
                    if(maxPop<fossilRecord[year][species]){maxPop=fossilRecord[year][species];}
                    ctx.lineTo(rx+(width*(i/fossilRecord.length)),ry+height-(fossilRecord[year][species]*height/maxPop));
                    i++;
                }
                ctx.fillStyle=species;
                ctx.fillText(fossilRecord[fossilRecord.length-1][species], rx+(width*(i/fossilRecord.length)), ry+height-(fossilRecord[fossilRecord.length-1][species]*height/maxPop));
                ctx.lineWidth=10;
                ctx.strokeStyle=species;
                ctx.stroke();
            }
        }
    }
    
}